import { Request, Response, ErrorRequestHandler, NextFunction } from 'express';

import { lang } from '../utils/lang'; // custom errors string
import { AppError, ServerError } from '../utils/app-errors'; // err from app

/**
 * Error-handling middleware. Sends error response to user. Sends server errors as request error.
 */
export default function (): ErrorRequestHandler {
  
  return function (err: Error, _req: Request, res: Response, _next: NextFunction): void {
    // if application server error send as request error
    if (err instanceof ServerError) {
      res.status(400).send({ error: { status: 400, message: lang['DE'].contact_forsa } });
      return;
    }
    // if other application error send as it is
    if (err instanceof AppError) {
      res.status(err.statusCode).send({ error: { status: err.statusCode, message: err.message } });
      return;
    }
    // if not uncaught error send as request error
    res.status(400).send({ error: { status: 400, message: lang['DE'].contact_forsa } });
  };
}
